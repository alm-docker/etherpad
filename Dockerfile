# Etherpad Lite Dockerfile
#
# Source https://github.com/ether/etherpad-docker
#
# Author of multi arch changes : alemaire
# Original author: muxator
#
# Version 0.2

FROM multiarch/alpine:_DISTCROSS-edge
MAINTAINER Adrien le Maire <adrien@alemaire.be>
RUN apk add --no-cache --update nodejs-current npm curl
# git hash of the version to be built.
# If not given, build the latest development version.
ARG VERSION_ID=master
# avoid installing devDeps
ENV NODE_ENV=production

# grab the VERSION_ID tarball from github (no need to clone the whole
# repository)
RUN echo "Getting version: ${VERSION_ID}" && \
	curl \
		--location \
		--fail \
		--silent \
		--show-error \
		--output /opt/etherpad-lite.tar.gz \
		https://github.com/ether/etherpad-lite/archive/"${VERSION_ID}".tar.gz && \
	mkdir /opt/etherpad-lite && \
	tar xf /opt/etherpad-lite.tar.gz \
		--directory /opt/etherpad-lite \
		--strip-components=1 && \
	rm /opt/etherpad-lite.tar.gz

# install node dependencies for Etherpad after settings to include custom plugins
RUN /opt/etherpad-lite/bin/installDeps.sh && \
    cd /opt/etherpad-lite/ && \
    npm install --no-save --loglevel warn ep_hash_auth ep_oidc ep_adminpads ep_hide_referrer

# Copy the custom configuration file, if present. The configuration file has to
# be manually put inside the same directory containing the Dockerfile (we cannot
# directly point to "../settings.json" for Docker's security restrictions).
#
# For the conditional COPY trick, see:
#   https://stackoverflow.com/questions/31528384/conditional-copy-add-in-dockerfile#46801962
COPY nop setting[s].json /opt/etherpad-lite/

EXPOSE 9001
WORKDIR /opt/etherpad-lite
CMD ["node", "node_modules/ep_etherpad-lite/node/server.js"]
